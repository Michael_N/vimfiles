set nocompatible
source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
behave mswin
filetype off                   " required!
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ==== vundle ====
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if has('win32') || has('win64')
  set rtp+=~/vimfiles/bundle/vundle/
  call vundle#rc('$HOME/vimfiles/bundle/')
else
  " Usual quickstart instructions
  set rtp+=~/.vim/bundle/vundle/
  call vundle#rc()
endif
" let Vundle manage Vundle - required! 
  Bundle 'gmarik/vundle'
" original repos on github
" Allgemeines
  Bundle 'tpope/vim-fugitive'
  Bundle 'vim-addon-mw-utils'
  Bundle 'Shougo/neocomplcache'
  Bundle 'Shougo/neosnippet'
  Bundle 'honza/snipmate-snippets'
  Bundle 'vimlatex'
  Bundle 'The-NERD-Commenter'
  Bundle 'majutsushi/tagbar'
  Bundle 'surround.vim'
  Bundle 'AutoClose'
  "Bundle 'UltiSnips'
  "
  Bundle 'LaTeX-Suite-aka-Vim-LaTeX'
 filetype plugin indent on     " required!
 "
 " Brief help
 " :BundleList          - list configured bundles
 " :BundleInstall(!)    - install(update) bundles
 " :BundleSearch(!) foo - search(or refresh cache first) for foo
 " :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
 "
 " see :h vundle for more details or wiki for FAQ
 " NOTE: comments after Bundle command are not allowed..
 "let g:neocomplcache_enable_at_startup = 1
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)

" SuperTab like snippets behavior.
imap <expr><TAB> neosnippet#expandable_or_jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For snippet_complete marker.
if has('conceal')
  set conceallevel=2 concealcursor=i
endif
"
" Tasten neu belegen
let mapleader=','
imap jj <Esc>
nnoremap � :
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Append modeline after last line in buffer.
" Use substitute() instead of printf() to handle '%%s' modeline in LaTeX
" files.
function! AppendModeline()
  let l:modeline = printf(" vim: set ts=%d sw=%d tw=%d :",
        \ &tabstop, &shiftwidth, &textwidth)
  let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
  call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>ml :call AppendModeline()<CR>

" Get that filetype stuff happening
filetype on
filetype plugin indent on
syntax on
set hidden
set lazyredraw
set showmode
let winManagerWindowLayout = "FileExplorer"
" general
colorscheme mn2012
set columns=85
set colorcolumn=80
set textwidth=79
set lines=35
set number
set ruler
set lbr!
set cmdheight=2	" Height of the command bar
set hid	" A buffer becomes hidden when it is abandoned
set wildmenu	" tab completion for command line
set autoread
set expandtab
set scrolloff=2	" number of lines to keep above and below the cursor
"set autoindent "Automatically try to indent
"set smartindent
set shiftround "Round the indention nicely with other indents
set hlsearch
set history=50	"keep 50 line command history
set tabstop=4
set softtabstop=2
set shiftwidth=2
autocmd BufRead *.py set tabstop=4
autocmd BufRead *.py set softtabstop=4
autocmd BufRead *.tex set shellslash
set expandtab  "Use spaces instead of tabs
" indent in msmode
vnoremap <Tab> >
vnoremap <S-Tab> <
set matchpairs=(:),{:},[:],<:>
if has("gui_running")
    "set guioptions=ac
    set guifont=Consolas:h11
endif
set guioptions-=r
" keine Backups ...
set nobackup
set nowritebackup
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ==== snipMate ====
"let g:snips_author = "Michael Niermann"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Bewegung bei splits
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" Faltungen mit Leertaste �ffnen + schlie�en
nnoremap <space> za
vnoremap <space> zf

" map ALT-Left and ALT-Right to move between tabs
map <silent><A-Right> :tabnext<CR>
map <silent><A-Left> :tabprevious<CR>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ==== Funktionstasten ====
"	F05 - Python/gcc ausf�hren / LaTeX - erweitern
"	F06 - ctags
"	F08 - Latex auf�hren und ansehen
"	F10 - Rechtschreibung Englisch
"	F11 - Rechtschreibung Deutsch
"	F12 - Rechtschreibung aus
"
autocmd FileType python map <F5> :w<CR>:!python "%"<CR>
autocmd FileType gle map <F5> :w<CR>:!gle "%"<CR>
autocmd FileType C map <F5> :w<CR>:!gcc -o %< %<CR>:! %<<CR>
nmap <F6> :TagbarToggle<CR>
autocmd FileType TEX map <F8> :w<CR>,ll<CR>,lv<CR>
map <F10> <Esc>:setlocal spell spelllang=en<CR>
map <F11> <Esc>:setlocal spell spelllang=de<CR>
map <F12> <Esc>:setlocal nospell<CR>

" ==== Python ====
autocmd FileType python set complete+=k~/.vim/syntax/python.vim isk+=.,(
iabbr _pyutf # -*- coding: utf8 -*-<CR>

" enable real code completion for python
" (needs vim to be build with python support)
autocmd FileType python set omnifunc=pythoncomplete#Complete
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" ==== Latex ====
let g:Tex_Leader=','
"
set grepprg=grep\ -nH\ $*
" Set the warning messages to ignore.
let g:Tex_IgnoredWarnings =
\"Underfull\n".
\"Overfull\n".
\"specifier changed to\n".
\"You have requested\n".
\"Missing number, treated as zero.\n".
\'LaTeX Font Warning:'"
"\"There were undefined references\n".
"\"Citation %.%# undefined\n".
" This number N says that latex-suite should ignore the first N of the above.
let g:Tex_IgnoreLevel = 6
" TIP: if you write your \label's as \label{fig:something}, then if you
" type in \ref{fig: and press Ctrl-N you will automatically cycle through
" all the figure labels. Very useful!
set iskeyword+=:
let g:Tex_HotKeyMappings='align,align*,figure'
let g:Tex_FoldedEnvironments="verbatim,comment,eq,gather,align,figure,table,thebibliography,keywords,abstract,titlepage,frame"
let g:Tex_Diacritics='0'
"let g:Tex_Env_theorem = "\\begin{theorem}\<CR><++>\<CR>\\end{theorem}"
let g:Tex_Env_figure = "\\begin{figure}[<+htpb+>]\<CR>\\centering\<CR>\\caption{<+caption text+>}\<CR>\\label{fig:<+label+>}\<CR>\\includegraphics{<+file+>}\<CR>\\end{figure}<++>"
let g:tex_flavor='latex'
" to get default compile to pdf
let g:Tex_DefaultTargetFormat = 'pdf'
let g:Tex_MultipleCompileFormats='pdf, aux'
let g:Tex_CompileRule_pdf = 'pdflatex -synctex=1 -interaction=nonstopmode $*'
let g:Tex_ViewRule_pdf = 'SumatraPDF.exe -reuse-instance'
